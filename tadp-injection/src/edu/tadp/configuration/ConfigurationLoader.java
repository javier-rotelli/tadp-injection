package edu.tadp.configuration;

import edu.tadp.configuration.entity.ObjectDefinition;
import edu.tadp.domain.Creador;
import edu.tadp.init.Context;


/**Contrato que firman los objetos que en algún momento harán de parser de la configuración.**/
public interface ConfigurationLoader {
	
	
	public Context getContext(Creador unCreador);
	
	public ObjectDefinition get(String nombre);

}
