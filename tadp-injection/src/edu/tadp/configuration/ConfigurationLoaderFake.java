package edu.tadp.configuration;

import java.util.Map;

import edu.tadp.configuration.entity.BuscadorDeObjetos;
import edu.tadp.configuration.entity.ObjectDefinition;
import edu.tadp.domain.Creador;
import edu.tadp.domain.impl.InyectorPorMuttator;
import edu.tadp.init.Context;

public class ConfigurationLoaderFake implements ConfigurationLoader {
	
	Map<String, ObjectDefinition> beansConfigurados;
	
	/**
	 * La obtención de los beans setteados en la configuración son LazyInitialization
	 * @param nombre es el nombre con el que encontramos el objeto dentro de la configuración
	 * @return la configuración del objeto en forma de {@link ObjectDefinition}
	 **/
	@Override
	public ObjectDefinition get(String nombre){
		
		ObjectDefinition beanPedido = this.getBeansConfigurados().get(nombre);
		
		if(beanPedido == null){
			this.obtenerDesdeConfiguracion(nombre);
		}
		
		return beanPedido;
		
	}

	public Map<String, ObjectDefinition> getBeansConfigurados() {
		return beansConfigurados;
	}

	public void setBeansConfigurados(Map<String, ObjectDefinition> beansConfigurados) {
		this.beansConfigurados = beansConfigurados;
	}
	
	
	/**
	 * TODO IMPLEMENTAR**/
	private ObjectDefinition obtenerDesdeConfiguracion(String nombre){
		return null;
	}


	@Override
	public Context getContext(Creador unCreador)
	{
		 return new Context(new BuscadorDeObjetos(new InyectorPorMuttator(), ConfigurationLoaderFake.this));
		 
	}

	
}
