package edu.tadp.configuration.entity;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import edu.tadp.configuration.ConfigurationLoader;
import edu.tadp.domain.Creador;
import edu.tadp.domain.Inyectador;
import edu.tadp.exceptions.ErrorEnLaConfiguracionException;
import edu.tadp.exceptions.NoPodesSerLasDosCosasException;
import edu.tadp.exceptions.TenesQueSerAlgoException;
import edu.tadp.exceptions.TuvisteProblemasParaInstanciarException;

public class BuscadorDeObjetos implements Inyectador {

	Creador creador;
	ConfigurationLoader beans;
	
	public BuscadorDeObjetos(Creador creador, ConfigurationLoader config){
		this.creador = creador;
		this.beans = config;
	}

	@Override
	public Object inyectar(String name) 
	{
		
		ObjectDefinition objeto = this.getBeans().get(name);
		
		return this.inyectar(objeto.getClase(), objeto.getPropiedades(),
				new LinkedHashMap<String, Object>());
	}

	/**
	 * @param name nombre de la clase del objeto a mapear
	 * @param propiedades son las propiedades que tiene mapeadas en el archivo de configuración este objeto
	 * @param propiedadesAMapear son las propiedades "reales", si la propiedad era una referencia a otro objeto, es el objeto con todas sus propiedades
	 * 
	 * @return el objeto a inyectar (luedo de delegárselo al mapeador)
	 * @throws TenesQueSerAlgoException 
	 * 
	 * @throws NoPodesSerLasDosCosasException y TenesQueSerAlgoException en base al valor de las propiedades**/
	private Object inyectar(String name, List<Property> propiedades,
			final LinkedHashMap<String, Object> propiedadesAMapear)
	{

		for(Property propiedad : propiedades)
		{
			this.validarDatosPropiedades(propiedad);
			if(propiedad.esValor() || propiedad.esColeccion())
				try {
					propiedadesAMapear.put(propiedad.getNombre(), this.getInstancia(Class.forName(name), propiedad));
				} catch (ClassNotFoundException e) {
					
					e.printStackTrace();
					throw new ErrorEnLaConfiguracionException("No encuentro La clase "+ name);
				}
			else
					propiedadesAMapear.put(propiedad.getNombre(), this.inyectar(propiedad.getReferencia()));
				
		}

		try {
			return this.getMapeador().instanciar(Class.forName(name),
					propiedadesAMapear);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new ErrorEnLaConfiguracionException("No encuentro La clase "+ name);
		}

	}

	/**
	 * @param <T>
	 * @param propiedad de la cual generara la instancia
	 * @return la instancia viva
	 */
	private Object getInstancia(Class<?> clase, Property propiedad) 
	{
		Object instanciaViva;
		
		Class<?> tipo;
		try {
			tipo = clase.getDeclaredField(propiedad.getNombre()).getType();
		} catch (Exception e1) {
			throw new ErrorEnLaConfiguracionException("Te está faltando un field o hiciste lío con la seguridad en la declaración de " + clase.getName());
		}
		
		if(Collection.class.isAssignableFrom(tipo))
		{
			
			Collection<Object> coleccion;
			try {
				coleccion = (Collection<Object>)Class.forName(tipo.getName()).newInstance();
			} catch (Exception e) {
				throw new TuvisteProblemasParaInstanciarException("Estás queriendo instanciar "+ tipo.getName() +", asegurate que eso esté bien");
			}
			  
			for(ObjectDefinition elemento : propiedad.getLista())
			{
				coleccion.add(this.inyectar(elemento.getClase(),elemento.getPropiedades(),new LinkedHashMap<String, Object>()));
			}
			
			instanciaViva = coleccion;
		}
		else 
		{	//puede ser una clase nativa de java, o una primitiva, intentamos castearla
			
			try
			{
				Constructor<?> constructor =  tipo.getDeclaredConstructor(String.class);
				try {
					instanciaViva = constructor.newInstance(propiedad.getValor());
				} catch (Exception e) {
					
					e.printStackTrace();
					throw new TuvisteProblemasParaInstanciarException("Asegurate que el constructor que elegiste de la clase "+ tipo.getName()+" acepte el valor que tenés en "+propiedad.getNombre()); 
					
				}
			}
			catch(NoSuchMethodException e)	//si no puedo construir un objeto de este tipo, asumo que el valor es una clase, e instancio acordemente 
			{
				try {
					instanciaViva = Class.forName(propiedad.getValor()).newInstance();
				} catch (Exception e1) {
					e1.printStackTrace();
					throw new ErrorEnLaConfiguracionException("No pudimos intanciar la propiedad " + propiedad.getNombre()+ ", verificá tu configuración");
				} 
			}
		}
		return instanciaViva;
	}
	
	/**
	 * @param propiedad es la propiedad a evaluar
	 * @return No tiene retorno, pero explota si la propiedad tiene referencia y valor, o no tiene ninguna de las dos (sólo puede tener una)
	 **/
	private void validarDatosPropiedades(Property propiedad)
			throws NoPodesSerLasDosCosasException, TenesQueSerAlgoException {
		if (propiedad.esReferencia() && propiedad.esValor()) {
			throw new NoPodesSerLasDosCosasException(propiedad.getNombre());
		}

		if (!propiedad.esReferencia() && !propiedad.esValor() && !propiedad.esColeccion()) {
			throw new TenesQueSerAlgoException(propiedad.getNombre());
		}
	}

	public Creador getMapeador() {
		return creador;
	}

	public void setMapeador(Creador mapeador) {
		this.creador = mapeador;
	}

	public ConfigurationLoader getBeans() {
		return beans;
	}

	public void setBeans(ConfigurationLoader beans) {
		this.beans = beans;
	}
	
	

}
