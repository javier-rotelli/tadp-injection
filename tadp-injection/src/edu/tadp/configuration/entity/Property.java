package edu.tadp.configuration.entity;

import java.util.ArrayList;
import java.util.Collection;

public class Property 
{
	String nombre;
	String valor;
	String referencia;
	String clase;
	Collection<ObjectDefinition> lista;
	
	public Property(Collection<ObjectDefinition> collection){
		this.lista = collection;
	}
	public Property(){
	}
	
	public String getNombre() 
	{
		return nombre;
	}
	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}
	public String getValor() 
	{
		return valor;
	}
	public void setValor(String valor) 
	{
		this.valor = valor;
	}
	public String getReferencia() 
	{
		return referencia;
	}
	public void setReferencia(String referencia) 
	{
		this.referencia = referencia;
	}

	public Boolean esValor(){
		return this.getValor()!=null;
	}
	
	public Boolean esReferencia()
	{
		return this.getReferencia()!=null;
	}
	
	public Collection<ObjectDefinition> getLista() 
	{
		return this.lista;
	}
	
	public void setLista(Collection<ObjectDefinition> lista)
	{
		this.lista = lista;
	}
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	public boolean esColeccion() 
	{
		return this.getLista() != null;
	}
	
	public void addToTheList(ObjectDefinition object){
		if(this.lista == null) this.lista = new ArrayList<ObjectDefinition>();
		
		lista.add(object);
	}
}
