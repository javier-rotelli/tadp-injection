package edu.tadp.configuration.entity;

import java.util.ArrayList;
import java.util.List;

public class ObjectDefinition
{
	String nombre;
	String clase;
	List<Property> propiedades;
	
	
	public ObjectDefinition(){
	}
	
	public ObjectDefinition(String nombre){
		this.nombre = nombre;
	}
	
	public void addProperty(Property propiedad)
	{
		if(propiedades == null)
			propiedades = new ArrayList<Property>();
		
		propiedades.add(propiedad);
	}
	
	public void setPropiedades(ArrayList<Property> listaPropiedades){
		propiedades = listaPropiedades;
	}

	public List<Property> getPropiedades() 
	{
		return propiedades;
	}

	public String getClase() 
	{
		return clase;
	}

	public void setClase(String clase) 
	{
		this.clase = clase;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
