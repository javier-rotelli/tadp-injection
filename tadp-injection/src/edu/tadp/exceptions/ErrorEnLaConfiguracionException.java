package edu.tadp.exceptions;

public class ErrorEnLaConfiguracionException extends RuntimeException 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7388780447804521252L;
	
	public ErrorEnLaConfiguracionException(String mensaje) 
	{
		super(mensaje);
	}

}
