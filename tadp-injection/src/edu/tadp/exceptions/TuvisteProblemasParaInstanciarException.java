package edu.tadp.exceptions;

public class TuvisteProblemasParaInstanciarException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2376150994667007121L;

	public TuvisteProblemasParaInstanciarException(String mensaje)
	{
		super(mensaje);
	}

}
