package edu.tadp.domain;

import java.lang.reflect.InvocationTargetException;

import edu.tadp.exceptions.NoPodesSerLasDosCosasException;
import edu.tadp.exceptions.TenesQueSerAlgoException;


public interface Inyectador {
	
	/**
	 * @param nombre es el nombre del objeto que se tiene que buscar en la configuración
	 * 
	 * @return el objeto pedido por el usuario del framework con todas sus dependencias resueltas (las que tiene mapeadas en la configuración)
	 * 
	 * @throws tiene miles excepciones, que deberán ser interceptadas para que el usuario del framework reciba mensajes bonitos.
	 **/	
	public Object inyectar(String nombre) throws SecurityException, IllegalArgumentException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException, ClassNotFoundException, NoPodesSerLasDosCosasException, TenesQueSerAlgoException ;

}
