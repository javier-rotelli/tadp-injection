package edu.tadp.domain;

import java.util.LinkedHashMap;

public interface Creador 
{
		
	public Object instanciar(Class<?> clase, LinkedHashMap<String, Object> propiedades);

}
