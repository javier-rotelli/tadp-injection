package edu.tadp.domain.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;

import edu.tadp.domain.Creador;
import edu.tadp.exceptions.ErrorEnLaConfiguracionException;


public abstract class InyectorConstructorDefault implements Creador 
{

	@Override
	public Object instanciar(Class<?> clase, LinkedHashMap<String, Object> propiedades) 
	{
		Object objeto;
		try 
		{
			objeto = clase.newInstance();
			for (Map.Entry<String, Object> propiedad : propiedades.entrySet())
			{
				this.inyectar(objeto, propiedad.getKey(), propiedad.getValue());
			}
			return objeto;
		} 
		catch (Exception e) 
		{
			throw new ErrorEnLaConfiguracionException(e.getMessage());
		}
		
	}
	
	protected abstract Object inyectar(Object objeto, String clave, Object valor) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;

}