package edu.tadp.domain.impl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class InyectorPorProperties extends InyectorConstructorDefault 
{

	@Override
	protected Object inyectar(Object objeto, String clave, Object valor)
			throws SecurityException, NoSuchMethodException,IllegalArgumentException, IllegalAccessException,InvocationTargetException, NoSuchFieldException 
	{
		Field property = objeto.getClass().getDeclaredField( clave );
		property.setAccessible(true);
		property.set(objeto, valor);
			
		return objeto;
	}

}
