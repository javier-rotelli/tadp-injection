package edu.tadp.domain.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InyectorPorMuttator extends InyectorConstructorDefault
{
	/**
	 * inyecta las propiedades dades en objeto
	 * @param objeto
	 * @param propiedades un map con las propiedades a instanciar
	 * @return
	 * @throws NoSuchMethodException 
	 * @throws SecurityException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	protected Object inyectar(Object objeto, String clave, Object valor) 
			throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException 
	{
	
		String nombreMetodo = "set" + clave.substring(0,1).toUpperCase() + clave.substring(1);
		Method metodo = objeto.getClass().getMethod(nombreMetodo, valor.getClass());
		metodo.invoke(objeto, valor);		
		
		return objeto;
	}
}
