package edu.tadp.domain.impl;

import ch.lambdaj.function.convert.Converter;

@SuppressWarnings("rawtypes")
public class ToClassConverter implements Converter<Object, Class> 
{

	@Override
	public Class convert(Object o) 
	{
		return o.getClass();
	}

}
