package edu.tadp.domain.impl;

import static ch.lambdaj.Lambda.convert;

import java.lang.reflect.Constructor;
import java.util.LinkedHashMap;

import edu.tadp.domain.Creador;
import edu.tadp.exceptions.ErrorEnLaConfiguracionException;

public class InyectorConstructorParametrizado implements Creador 
{

	@Override
	public Object instanciar(Class<?> clase, LinkedHashMap<String, Object> propiedades)
	{
		
		Constructor<?> con;
		try 
		{
			con = clase.getConstructor( (Class<?>[]) convert(propiedades.values(),new ToClassConverter()).toArray(new Class<?>[0]) );
			return con.newInstance(propiedades.values().toArray());
		} 
		catch (Exception e) 
		{
			throw new ErrorEnLaConfiguracionException(e.getMessage());
		} 
				
	}

}
