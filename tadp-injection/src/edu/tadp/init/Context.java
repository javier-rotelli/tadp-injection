package edu.tadp.init;

import edu.tadp.configuration.entity.BuscadorDeObjetos;
import edu.tadp.domain.Inyectador;

public class Context implements Inyectador {
	
	BuscadorDeObjetos buscadorDeBeans;
	
	public Context(	BuscadorDeObjetos buscadorDeBeans){
		this.buscadorDeBeans = buscadorDeBeans;
	}

	@Override
	public Object inyectar(String nombre) {
		
		return buscadorDeBeans.inyectar(nombre);
		
	}

	public BuscadorDeObjetos getBuscadorDeBeans() {
		return buscadorDeBeans;
	}

	public void setBuscadorDeBeans(BuscadorDeObjetos buscadorDeBeans) {
		this.buscadorDeBeans = buscadorDeBeans;
	}
	
	
	
	

}
