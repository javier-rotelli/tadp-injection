package edu.tadp.domain.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import edu.tadp.configuration.entity.ObjectDefinition;
import edu.tadp.configuration.entity.Property;
import edu.tadp.domain.Creador;
import edu.tadp.domain.impl.test.PersonaDatabaseHome;

public class ListaLoaderTest extends BuscadorDeObjetosTest {

	@Override
	public Creador getCreador() 
	{
		return new InyectorPorMuttator();
	}

	@Override
	public LinkedHashMap<String, Object> GetPropiedadesParaTestear() 
	{
		LinkedHashMap<String, Object> propiedades = new LinkedHashMap<String, Object>(); 
		propiedades.put("usuario", "admin");
		propiedades.put("password", "Pa$$w0rd");
		propiedades.put("host", "(localhost)");
		propiedades.put("puerto", 8080);
		propiedades.put("esquema", "esquemaGenerado");

		Collection<PersonaDatabaseHome> descendientes = new ArrayList<PersonaDatabaseHome>();
		for (int i = 0; i < 20; i++)
		{
			PersonaDatabaseHome persona = new PersonaDatabaseHome();
			persona.setUsuario("admin");
			persona.setPassword("Pa$$w0rd");
			persona.setHost("(localhost)");
			persona.setPuerto(8080);
			persona.setEsquema("esquemaGenerado");
			descendientes.add(persona);
		}
		propiedades.put("descendientes", descendientes);
		
		return propiedades;
	}

	@Override
	public Boolean verificarResultado(PersonaDatabaseHome personaDatabaseHome) 
	{
		PersonaDatabaseHome primerDescendiente = personaDatabaseHome.getDescendientes().iterator().next();
		
		return
				personaDatabaseHome.getUsuario().equals(propiedades.get("usuario"))
				&& personaDatabaseHome.getPassword().equals(propiedades.get("password"))
				&& personaDatabaseHome.getHost().equals(propiedades.get("host"))
				&& personaDatabaseHome.getPuerto().equals(propiedades.get("puerto"))
				&& personaDatabaseHome.getEsquema().equals(propiedades.get("esquema"))
				&& personaDatabaseHome.getDescendientes().size() == 20
				&& primerDescendiente.getUsuario().equals(propiedades.get("usuario"))
				&& primerDescendiente.getPassword().equals(propiedades.get("password"))
				&& primerDescendiente.getHost().equals(propiedades.get("host"))
				&& primerDescendiente.getPuerto().equals((Integer)propiedades.get("puerto"))
				&& primerDescendiente.getEsquema().equals(propiedades.get("esquema"));		
	}

	@Override
	protected String getBeanName() 
	{
		return "beanConLista";
	}
	
	@Override
	protected Collection<Property> getPropiedadesParaObjectDefinition() 
	{
		Collection<Property> propiedades = new ArrayList<Property>(); 
		propiedades.add(this.CrearPropiedad("usuario", "admin", "", null));
		propiedades.add(this.CrearPropiedad("password", "Pa$$w0rd", "", null));
		propiedades.add(this.CrearPropiedad("host", "(localhost)", "", null));
		propiedades.add(this.CrearPropiedad("puerto", "8080", "", null));
		propiedades.add(this.CrearPropiedad("esquema", "esquemaGenerado", "", null));
		
		List<ObjectDefinition> personas = new ArrayList<ObjectDefinition>();
		
		for (int i = 0; i < 20; i++) 
		{
			ObjectDefinition props = new ObjectDefinition();
			props.addProperty(this.CrearPropiedad("usuario", "admin", "", null));
			props.addProperty(this.CrearPropiedad("password", "Pa$$w0rd", "", null));
			props.addProperty(this.CrearPropiedad("host", "(localhost)", "", null));
			props.addProperty(this.CrearPropiedad("puerto", "8080", "", null));
			props.addProperty(this.CrearPropiedad("esquema", "esquemaGenerado", "", null));
			props.setClase(this.getClaseNombre());
			personas.add(props);
			
		}
		propiedades.add(this.CrearPropiedad("descendientes", "", "", personas));
		
		return propiedades;
	}

	@Override
	protected String getClaseNombre() 
	{
		return "edu.tadp.domain.impl.test.PersonaDatabaseHome";
	}

}
