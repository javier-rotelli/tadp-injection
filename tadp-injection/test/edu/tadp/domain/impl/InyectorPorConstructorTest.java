package edu.tadp.domain.impl;


import edu.tadp.domain.Creador;


public class InyectorPorConstructorTest extends MapeadorTest 
{

	@Override
	public Creador getCreador() 
	{
		return new InyectorConstructorParametrizado();
	}


}
