package edu.tadp.domain.impl.test;

import java.util.List;

public interface PersonaHome 
{
	List<Object> getPersonas();
}
