package edu.tadp.domain.impl.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PersonaDatabaseHome implements PersonaHome
{
	String usuario;
	String password;
	String host;
	Integer puerto;
	String esquema;
	
	PersonaDatabaseHome primo;
	ArrayList<PersonaDatabaseHome> descendientes;
	
		public PersonaDatabaseHome()
	{
		super();
	}
	
	public PersonaDatabaseHome(String usuario, String password, String host, Integer puerto, String esquema) 
	{
		super();
		this.usuario = usuario;
		this.password = password;
		this.host = host;
		this.puerto = puerto;
		this.esquema = esquema;
	}
	
	public PersonaDatabaseHome(String usuario, String password, String host, Integer puerto, String esquema, PersonaDatabaseHome primo) 
	{
		super();
		this.usuario = usuario;
		this.password = password;
		this.host = host;
		this.puerto = puerto;
		this.esquema = esquema;
		this.primo = primo;
	}

	public String getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(String usuario) 
	{
		this.usuario = usuario;
	}

	public String getPassword() 
	{
		return password;
	}

	public void setPassword(String password) 
	{
		this.password = password;
	}

	public String getHost() 
	{
		return host;
	}

	public void setHost(String host) 
	{
		this.host = host;
	}

	public Integer getPuerto() 
	{
		return puerto;
	}

	public void setPuerto(Integer puerto) 
	{
		this.puerto = puerto;
	}

	public String getEsquema() 
	{
		return esquema;
	}

	public void setEsquema(String esquema) 
	{
		this.esquema = esquema;
	}
	
	public void setPrimo(PersonaDatabaseHome primo)
	{
		this.primo = primo;
	}
	
	public PersonaDatabaseHome getPrimo()
	{
		return this.primo;
	}

	@Override
	public List<Object> getPersonas() 
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	public Collection<PersonaDatabaseHome> getDescendientes() 
	{
		return descendientes;
	}

	public void setDescendientes(ArrayList<PersonaDatabaseHome> descendientes) 
	{
		this.descendientes = descendientes;
	}


		
}
