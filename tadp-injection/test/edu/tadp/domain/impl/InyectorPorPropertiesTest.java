package edu.tadp.domain.impl;

import edu.tadp.domain.Creador;


public class InyectorPorPropertiesTest extends MapeadorTest 
{
	@Override
	public Creador getCreador() 
	{
		return new InyectorPorProperties();
	}
}
