package edu.tadp.domain.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import edu.tadp.configuration.entity.ObjectDefinition;
import edu.tadp.configuration.entity.Property;
import edu.tadp.domain.Creador;
import edu.tadp.domain.impl.test.PersonaDatabaseHome;

public class ObjetosLoader extends BuscadorDeObjetosTest 
{

	@Override
	public Creador getCreador() 
	{
		return new InyectorPorMuttator();
	}
	
	@Override
	public Map<String, ObjectDefinition> getBeansTest()
	{
		Map<String, ObjectDefinition> mapa = super.getBeansTest();
		mapa.put("beanPrimo", this.getPrimo());
		return mapa;
	}

	private ObjectDefinition getPrimo() 
	{
		ObjectDefinition objectDefinition = new ObjectDefinition();
		objectDefinition.setClase(this.getClaseNombre());
		objectDefinition.addProperty(this.CrearPropiedad("usuario", "ElPrimo", "", null));
		objectDefinition.addProperty(this.CrearPropiedad("password", "passwordDelPrimo", "", null));
		objectDefinition.addProperty(this.CrearPropiedad("host", "UnoRemoto", "", null));
		objectDefinition.addProperty(this.CrearPropiedad("esquema", "ElPrimo", "", null));
		objectDefinition.addProperty(this.CrearPropiedad("puerto", "22", "", null));
		return objectDefinition;
	}

	@Override
	protected Collection<Property> getPropiedadesParaObjectDefinition() 
	{
		Collection<Property> propiedades = new ArrayList<Property>(); 
		propiedades.add(this.CrearPropiedad("usuario", "admin", "", null));
		propiedades.add(this.CrearPropiedad("password", "Pa$$w0rd", "", null));
		propiedades.add(this.CrearPropiedad("host", "(localhost)", "", null));
		propiedades.add(this.CrearPropiedad("puerto", "8080", "", null));
		propiedades.add(this.CrearPropiedad("esquema", "esquemaGenerado", "", null));
		propiedades.add(this.CrearPropiedad("primo", "", "beanPrimo", null));
		return propiedades;
	}

	@Override
	protected String getClaseNombre() 
	{
		return "edu.tadp.domain.impl.test.PersonaDatabaseHome";
	}

	@Override
	protected String getBeanName() 
	{
		return "beanPersonaDBHome";
	}

	@Override
	public LinkedHashMap<String, Object> GetPropiedadesParaTestear() 
	{
		LinkedHashMap<String, Object> propiedades = new LinkedHashMap<String, Object>();
		propiedades.put("usuario", "admin");
		propiedades.put("password", "Pa$$w0rd");
		propiedades.put("host", "(localhost)");
		propiedades.put("puerto", 8080);
		propiedades.put("esquema", "esquemaGenerado");
		return propiedades;
	}
	
	@Override
	public Boolean verificarResultado(PersonaDatabaseHome objetoDePrueba) 
	{
		return
			objetoDePrueba.getUsuario().equals(propiedades.get("usuario"))
			&& objetoDePrueba.getPassword().equals(propiedades.get("password"))
			&& objetoDePrueba.getHost().equals(propiedades.get("host"))
			&& objetoDePrueba.getPuerto().equals((Integer)propiedades.get("puerto"))
			&& objetoDePrueba.getEsquema().equals(propiedades.get("esquema"))
			&& objetoDePrueba.getPrimo() != null
			&& objetoDePrueba.getPrimo().getUsuario().equals("ElPrimo")
			&& objetoDePrueba.getPrimo().getPassword().equals("passwordDelPrimo")
			&& objetoDePrueba.getPrimo().getHost().equals("UnoRemoto")
			&& objetoDePrueba.getPrimo().getEsquema().equals("ElPrimo")
			&& objetoDePrueba.getPrimo().getPuerto().equals(22);
	}





}
