package edu.tadp.domain.impl;

import edu.tadp.domain.Creador;


public class InyectorPorMuttatorTest extends MapeadorTest
{
	
	@Override
	public Creador getCreador() 
	{
		return new InyectorPorMuttator();
	}

}
