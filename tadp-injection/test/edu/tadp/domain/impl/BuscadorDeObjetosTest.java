package edu.tadp.domain.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.tadp.configuration.ConfigurationLoader;
import edu.tadp.configuration.ConfigurationLoaderFake;
import edu.tadp.configuration.entity.BuscadorDeObjetos;
import edu.tadp.configuration.entity.ObjectDefinition;
import edu.tadp.configuration.entity.Property;
import edu.tadp.domain.Creador;
import edu.tadp.domain.impl.test.PersonaDatabaseHome;


public abstract class BuscadorDeObjetosTest 
{
	BuscadorDeObjetos instanciador;
	protected LinkedHashMap<String, Object> propiedades;
	
	public BuscadorDeObjetos getInstanciador()
	{
		return instanciador;
	}
	
	public abstract Creador getCreador();
	public abstract LinkedHashMap<String, Object> GetPropiedadesParaTestear();
	public abstract Boolean verificarResultado(PersonaDatabaseHome personaDatabaseHome);
	protected abstract String getBeanName();
	protected abstract Collection<Property> getPropiedadesParaObjectDefinition();
	protected abstract String getClaseNombre();
	
	public ConfigurationLoader getConfigurationLoader()
	{
		ConfigurationLoaderFake loader = new ConfigurationLoaderFake();
		loader.setBeansConfigurados(this.getBeansTest());
		return loader;
	}
	
	public Map<String, ObjectDefinition> getBeansTest() 
	{
		Map<String, ObjectDefinition> beansMap = new LinkedHashMap<String, ObjectDefinition>();
		beansMap.put(this.getBeanName(), this.crearObjectDefinition());
		return beansMap;
	}
	
	private ObjectDefinition crearObjectDefinition() 
	{
		ObjectDefinition objectDefinition = new ObjectDefinition();
		objectDefinition.setClase(this.getClaseNombre());
		for(Property propiedad : getPropiedadesParaObjectDefinition())
		{
			objectDefinition.addProperty(propiedad);
		}
		
		return objectDefinition;
	}
	
	protected Property  CrearPropiedad(String nombre, String valor, String referencia, Collection<ObjectDefinition> lista)
	{
		Property property =  new Property();
		if(nombre != "")
			property.setNombre(nombre);
		if(valor != "")
			property.setValor(valor);
		if(referencia != "")
			property.setReferencia(referencia);
		if(lista != null)
			property.setLista(lista);
		
		return property;
	}
	
	
	
	
	@Before
	public void setUp() throws Exception 
	{
		instanciador = new BuscadorDeObjetos(this.getCreador(), this.getConfigurationLoader());
		propiedades = this.GetPropiedadesParaTestear(); 
	}
	
	@After
	public void tearDown() throws Exception 
	{
		
	}
	
	@Test
	public void testCorrer()
	{
		try 
		{
			PersonaDatabaseHome objetoDePrueba = (PersonaDatabaseHome)this.getInstanciador().inyectar(this.getBeanName());
			assertEquals("", true, this.verificarResultado(objetoDePrueba));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			fail();
			
		}
	}
}
