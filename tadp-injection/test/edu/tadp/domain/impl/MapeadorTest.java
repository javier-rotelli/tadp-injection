package edu.tadp.domain.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.LinkedHashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.tadp.domain.Creador;
import edu.tadp.domain.impl.test.PersonaDatabaseHome;
import edu.tadp.exceptions.ErrorEnLaConfiguracionException;

public abstract class MapeadorTest {

	protected Creador inyector;
	protected LinkedHashMap<String, Object> propiedades;

	@Before
	public void setUp() throws Exception 
	{
		inyector = this.getCreador();
		propiedades = new LinkedHashMap<String, Object>();
		propiedades.put("usuario", "Juan");
		propiedades.put("password", "1234");
		propiedades.put("host", "elServer");
		propiedades.put("puerto", 24);
		propiedades.put("esquema", "unSchema");
		
	}

	@After
	public void tearDown() throws Exception 
	{
		propiedades.clear();
	}
	
	public abstract Creador getCreador();
	

	
	@Test
	public void testInstanciarClasePedida() {
		PersonaDatabaseHome objetoDePrueba;
		try 
		{
			objetoDePrueba = (PersonaDatabaseHome) inyector.instanciar(PersonaDatabaseHome.class, propiedades);
			assertTrue(objetoDePrueba.getClass() == PersonaDatabaseHome.class);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			fail();
		} 
		
	}

	@Test
	public void testInstanciarPropiedades() 
	{
		try 
		{
			PersonaDatabaseHome objetoDePrueba = (PersonaDatabaseHome) inyector.instanciar(PersonaDatabaseHome.class, propiedades);
			
		    assertEquals("", propiedades.get("usuario"), objetoDePrueba.getUsuario());
		    assertEquals("", propiedades.get("password"), objetoDePrueba.getPassword());
		    assertEquals("", propiedades.get("host"), objetoDePrueba.getHost());
		    assertEquals("", propiedades.get("puerto"), objetoDePrueba.getPuerto());
		    assertEquals("", propiedades.get("esquema"), objetoDePrueba.getEsquema());
		} 
		catch (Exception e) 
		{
			fail();
			e.printStackTrace();
		}
	}
	
	@Test(expected=ErrorEnLaConfiguracionException.class)
	public void testInstanciarPropiedadDeMas() 
	{
		propiedades.put("noExiste", "Lala");
		inyector.instanciar(PersonaDatabaseHome.class, propiedades);
	}

}