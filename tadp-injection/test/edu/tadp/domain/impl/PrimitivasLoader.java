package edu.tadp.domain.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;

import edu.tadp.configuration.entity.Property;
import edu.tadp.domain.Creador;
import edu.tadp.domain.impl.test.PersonaDatabaseHome;

public class PrimitivasLoader extends BuscadorDeObjetosTest 
{

	@Override
	public Creador getCreador() 
	{
		return new InyectorConstructorParametrizado();
	}

	@Override
	protected String getBeanName()
	{
		return "beanPersonaDBHome"; 
	}
	
	@Override
	protected  String getClaseNombre()
	{
		return "edu.tadp.domain.impl.test.PersonaDatabaseHome";
	}
	
	@Override
	protected Collection<Property> getPropiedadesParaObjectDefinition()
	{
		Collection<Property> propiedades = new ArrayList<Property>(); 
		propiedades.add(this.CrearPropiedad("usuario", "Juan", "", null));
		propiedades.add(this.CrearPropiedad("password", "1234", "", null));
		propiedades.add(this.CrearPropiedad("host", "elServer", "", null));
		propiedades.add(this.CrearPropiedad("puerto", "24", "", null));
		propiedades.add(this.CrearPropiedad("esquema", "unSchema", "", null));
		return propiedades;
	}
	
	@Override
	public LinkedHashMap<String, Object> GetPropiedadesParaTestear()
	{  
		LinkedHashMap<String, Object> propiedades = new LinkedHashMap<String, Object>();
		propiedades.put("usuario", "Juan");
		propiedades.put("password", "1234");
		propiedades.put("host", "elServer");
		propiedades.put("puerto", 24);
		propiedades.put("esquema", "unSchema");
		return propiedades;
	}
	
	@Override
	public Boolean verificarResultado(PersonaDatabaseHome personaDatabaseHome) 
	{
		return
				personaDatabaseHome.getUsuario().equals(propiedades.get("usuario"))
				&& personaDatabaseHome.getPassword().equals(propiedades.get("password"))
				&& personaDatabaseHome.getHost().equals(propiedades.get("host"))
				&& personaDatabaseHome.getPuerto().equals((Integer)propiedades.get("puerto"))
				&& personaDatabaseHome.getEsquema().equals(propiedades.get("esquema"));
	}
		
	
	
}